const rewardImages = document.querySelectorAll(".reward-image");
const rewardInfos = document.querySelectorAll(".reward-info");

const next = document.querySelector("#next-reward");
const prev = document.querySelector("#prev-reward");
rewardImages.forEach((element) => {
  element.style.width = "0";
});
rewardInfos.forEach((element) => {
  element.style.transform = "scaleX(0)";
});

let position = 0;

rewardImages[position].style.width = "100%";
rewardInfos[position].style.transform = "scale(1)";

next.onclick = (e) => {
  position++;
  if (position > 7) position = 0;
  rewardImages.forEach((element) => {
    element.style.width = "0";
  });
  rewardInfos.forEach((element) => {
    element.style.transform = "scaleX(0)";
  });
  console.log("position", position);
  rewardImages[position].style.width = "100%";
  rewardInfos[position].style.transform = "scale(1)";
};

prev.onclick = (e) => {
  position--;
  if (position < 0) position = 7;
  console.log(position);
  rewardImages.forEach((element) => {
    element.style.width = "0";
  });
  rewardInfos.forEach((element) => {
    element.style.transform = "scaleX(0)";
  });
  rewardImages[position].style.width = "100%";
  rewardInfos[position].style.transform = "scale(1)";
};
