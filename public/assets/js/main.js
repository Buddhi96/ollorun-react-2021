const left = document.querySelector('#left');
const right = document.querySelector('#right');

const opportunity = document.querySelector('.opportunity');

const l_intro = left.querySelector('.intro');
const r_intro = right.querySelector('.intro');

const h_explore = left.querySelector('#h-explore');
const W_explore = right.querySelector('#w-explore');

const h_packs = left.querySelector('.h-packs');
const w_packs = right.querySelector('.w-packs');

const h_close = left.querySelector('.h-close');
const w_close = right.querySelector('.w-close');

h_explore.onclick = (e) => l_toggle();
W_explore.onclick = (e) => r_toggle();

h_close.onclick = (e) => l_toggle();
w_close.onclick = (e) => r_toggle();

function l_toggle() {
    left.classList.toggle('full-width-left');
    right.classList.toggle('zero-scale');
    l_intro.classList.toggle('zero-scale');
    h_packs.classList.toggle('full-scale');
    h_close.classList.toggle('zero-scale');
    opportunity.classList.toggle('mt');
}

function r_toggle() {
    right.classList.toggle('full-width-right');
    left.classList.toggle('zero-scale');
    r_intro.classList.toggle('zero-scale');
    w_packs.classList.toggle('full-scale');
    w_close.classList.toggle('zero-scale');
    opportunity.classList.toggle('mt');
}

