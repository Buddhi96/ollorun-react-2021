import React from "react";
import en from "../lang/en";
import fr from "../lang/fr";
import esp from "../lang/esp";
class Wpackages extends React.Component {
  constructor(props) {
    super(props);

   
      this.state = {
       
        ln: en,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            UK{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
              alt=""
            />
          </a>
        ),
        
      };
    }
    changelang(lan) {
      if (lan === "en") {
        this.setState({
          ln: en,
          flag: (
            <a
              className="nav-link dropdown-toggle"
              href="#"
              id="navbarDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              {" "}
              UK{" "}
              <img
                className="ml-1"
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
                alt=""
              />
            </a>
          ),
        });
       
      } else if (lan === "fr") {
        this.setState({
          ln: fr,
          flag: (
            <a
              className="nav-link dropdown-toggle"
              href="#"
              id="navbarDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              {" "}
              FR{" "}
              <img
                className="ml-1"
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/FR_d2vo8g.svg"
                alt=""
              />
            </a>
          ),
        });
      
      } else if (lan === "esp") {
        this.setState({
          ln: esp,
          flag: (
            <a
              className="nav-link dropdown-toggle"
              href="#"
              id="navbarDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              {" "}
              ESP{" "}
              <img
                className="ml-1"
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163562/OLLORUN_IMAGES/images/elements/ESP_ukit8i.svg"
                alt=""
              />
            </a>
          ),
        });
      
      }
    }
    render() {
        return( 
            <div>
            <nav className="navbar navbar-expand-lg fixed-top">
          <a className="navbar-brand">
            <img
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_150,f_auto,q_auto/v1598163568/OLLORUN_IMAGES/images/elements/logo01_ollorun_ovoawo.png"
              alt=""
            />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span>
              <img
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/ham_y9sttw.svg"
                alt=""
              />
            </span>
          </button>
          <div
            className="collapse navbar-collapse justify-content-end"
            id="navbarNav"
          >
            <ul className="navbar-nav">
              <li
                className="nav-item active m-auto"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a 
                  
                  href="/#pannel-0"
                >
                  {this.state.ln[0]} <span className="sr-only">(current)</span>
                </a>
              </li>
              <li
                className="nav-item m-auto"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  
                  href="/#pannel-1"
                >
                  {this.state.ln[1]}
                </a>
              </li>
              <li
                className="nav-item m-auto"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  
                  href="/#pannel-2"
                >
                  {this.state.ln[2]}
                </a>
              </li>
              <li
                className="nav-item"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  target="_blank"
                  smooth={true}
                  duration={1000}
                  className="nav-link"
                  href={this.state.ln[151]}
                >
                  {this.state.ln[150]}
                </a>
              </li>
              <li
                className="nav-item text-center"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  className="nav-link"
                  target="_BLANK"
                  href="https://training.ollorun.com/index"
                >
                  TRAINING
                </a>
              </li>
              <li
                className="nav-item text-center"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  className="nav-link join-btn"
                  target="_BLANK"
                  href="https://backoffice.ollorun.com/"
                >
                  {this.state.ln[3]}
                </a>
              </li>
              <li className="nav-item dropdown">
                {this.state.flag}
                <div
                  className="dropdown-menu"
                  aria-labelledby="navbarDropdown"
                  data-toggle="collapse"
                  data-target="#navbarNav"
                >
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("en")}
                  >
                    UK{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
                      alt=""
                    />
                  </a>
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("fr")}
                  >
                    FR{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/FR_d2vo8g.svg"
                      alt=""
                    />
                  </a>
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("esp")}
                  >
                    ESP{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163562/OLLORUN_IMAGES/images/elements/ESP_ukit8i.svg"
                      alt=""
                    />
                  </a>
                </div>
              </li>
            </ul>
          </div>
        </nav>
     
            <section className="pack-top ">
              <div className="container-fluid h-100 color-layer">
                <div className="row h-100 justify-content-center text-center">
                  <div className="col-sm-8 text-center my-auto">
                  <h1 className="text-align-center justify-content-center">
                      WELTH PACKAGES
                    </h1>
                    <h4>ENHANCE YOUR WEALTH.</h4>
                  </div>
                  
                </div>
              </div>
              <div class="row">
            <a target="_blank" href="https://store.ollorun.com/index.php?route=product/category&path=60_65">
            <div className="col-sm-2 to-store-btn">
              Vist to Store
            </div>
            </a>
          </div>
            </section>
            <section className="welth-pack-down">
              
            <div className="container mt-5">
                
                <div className="row packs-view">
                <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/w_600,f_auto,q_auto/v1619162302/ollorun-wealth-packs/Ollorun_W_Elite_jk6tze.png"
                                    alt="MASTERNODE ELITE"
                                  />
                                  <h5>MASTERNODE ELITE</h5>
                                 <div className="d-flex m-4 mb-3 mt-0"><p class="price">12,668.00€</p>
                                  <p className="pv">PV 6200</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60&product_id=42"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162302/ollorun-wealth-packs/Ollorun_W_Gold_x8xifq.png"
                                    alt="Masternode Gold"
                                  />
                                  <h5>Masternode Gold</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">6,334.00€</p>
                                  <p className="pv">PV 3000</p>
                                  </div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60&product_id=33"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162302/ollorun-wealth-packs/Ollorun_W_Platinum_ofrwfw.png"
                                    alt="Maternode Platinum"
                                  />
                                  <h5>Masternode Platinum</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">1,250.00€</p>
                                  <p className="pv">PV 600</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60&product_id=46"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162307/ollorun-wealth-packs/Ollorun_W_Silver_uhnd3q.png"
                                    alt="Masternode Silver"
                                  />
                                  <h5>Masternode Silver</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">335.00€</p>
                                  <p className="pv">PV 150</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60&product_id=44"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162304/ollorun-wealth-packs/Ollorun_W_Standard_qryyax.png"
                                    alt="Masternode Standard"
                                  />
                                  <h5>Masternode Standard</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">126.00€</p>
                                  <p className="pv">PV 70</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60&product_id=43"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162304/ollorun-wealth-packs/Ollorun_W_Premium_z4ighl.png"
                                    alt="Maternode Premium"
                                  />
                                  <h5>Maternode Premium</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">620.00€</p>
                                  <p className="pv">PV 300</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60&product_id=45"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                          
                </div>
            </div>
            <div className="container-fluid pack-top mt-5 ">
            <div className="row text-center color-layer  h-100">
                    <h1 className="my-auto">Wealth OZTCARD</h1>
                </div>
                <div class="row">
            <a target="_blank" href="https://store.ollorun.com/index.php?route=product/category&path=60_64">
            <div className="col-sm-2 to-store-btn">
              Vist to Store
            </div></a>
          </div>
            </div>
            
            <div className="container mt-5">
                
                <div className="row packs-view">
                <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162307/ollorun-wealth-packs/ollorun_OZTcard_standard_black_lifqb0.png"
                                    alt="OLLORUN PREMIUM SMARTCARD By OZTCARD"
                                  />
                                  <h5>PREMIUM SMARTCARD</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">250€</p>
                                  <p className="pv">PV 150</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_64&product_id=71"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                              <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162306/ollorun-wealth-packs/ollorun_OZTcard_premium_kbsakx.png"
                                    alt="OLLORUN PREMIUM SMARTCARD By OZTCARD Health Edition"
                                  />
                                  <h5>PREMIUM SMARTCARD Health Edition</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">250.00€</p>
                                  <p className="pv">PV 150</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_64&product_id=70"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                              <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162302/ollorun-wealth-packs/ollorun_OZTcard_standard_white_gakplz.png"
                                    alt="OLLORUN STANDARD SMARTCARD By OZTCARD"
                                  />
                                  <h5>STANDARD SMARTCARD</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">250.00€</p>
                                  <p className="pv">PV 90</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_64&product_id=65"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                              <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162307/ollorun-wealth-packs/ollorun_OZTcard_gold_vqlpkk.png"
                                    alt="OLLORUN GOLD SMARTCARD By OZTCARD"
                                  />
                                  <h5>GOLD SMARTCARD</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">999.00€</p>
                                  <p className="pv">PV 450</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_64&product_id=69"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                                  
                </div>
            </div>
            <div className="container-fluid pack-top mt-5">
            <div className="row text-center color-layer  h-100">
                    <h1 className="my-auto">Wealth P.I.L Packages</h1>
                </div>
                <div class="row">
                <a target="_blank" href="https://store.ollorun.com/index.php?route=product/category&path=60_59">
            <div className="col-sm-2 to-store-btn">
              Vist to Store
            </div>
            </a>
          </div>
            </div>
            <div className="container mt-5 mb-5">
                
                <div className="row packs-view align-content-center">
                <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162302/ollorun-wealth-packs/Ollorun_STANDARD_PIL_q0atiw.png"
                                    alt="PIL STANDARD"
                                  />
                                  <h5>PIL STANDARD</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">3,269.00€</p>
                                  <p className="pv">PV 375</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_59&product_id=57"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                              <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162302/ollorun-wealth-packs/Ollorun_PREMIUM_PIL_wjcdiy.png"
                                    alt="PIL PREMIUM"
                                  />
                                  <h5>PIL PREMIUM</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">16,343.00€</p>
                                  <p className="pv">PV 1875</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_59&product_id=56"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                              <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162308/ollorun-wealth-packs/Ollorun_GOLD_PIL_sirmsu.png"
                                    alt="PIL GOLD"
                                  />
                                  <h5>PIL GOLD</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">32,685.00€</p>
                                  <p className="pv">PV 3800</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_59&product_id=55"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                              <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162306/ollorun-wealth-packs/Ollorun_AMBASSADOR_PIL_i0nutn.png"
                                    alt="PIL AMBASSADOR"
                                  />
                                  <h5>PIL AMBASSADOR</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">65,370.00€</p>
                                  <p className="pv">PV 7600</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_59&product_id=32"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                              <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162304/ollorun-wealth-packs/STANDARD_PIL_MOBIL_3D_g9tbp4.png"
                                    alt="PIL NODE STANDARD"
                                  />
                                  <h5>PIL NODE STANDARD</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">375.00€</p>
                                  <p className="pv">PV 100</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_59&product_id=59"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                              <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162308/ollorun-wealth-packs/DUO_PIL_MOBIL_3D_yzrsvc.png"
                                    alt="PIL NODE DUO"
                                  />
                                  <h5>PIL NODE DUO</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">700.00€</p>
                                  <p className="pv">PV 200</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_59&product_id=64"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                              <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162309/ollorun-wealth-packs/FAMILY_PIL_MOBIL_3D_zagiyd.png"
                                    alt="PIL NODE FAMILY"
                                  />
                                  <h5>PIL NODE FAMILY</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">999.00€</p>
                                  <p className="pv">PV 400</p></div>
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_59&product_id=61"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                              <div className="col-6 col-sm-3 mt-5">
                                <div class="card text-center product ">
                                  <img className="card_img"
                                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_600/v1619162305/ollorun-wealth-packs/ENTREPRENEUR_PIL_MOBIL_3D_k2sl9a.png"
                                    alt="PIL NODE FAMILY"
                                  />
                                  <h5>PIL NODE ENTREPRENEUR</h5>
                                  <div className="d-flex m-4 mb-3 mt-0">  <p class="price">2 500.00€</p>
                                  <p className="pv">PV 1200</p></div>
                                  
                                  <a
                                    target="_blank"
                                    href="https://store.ollorun.com/index.php?route=product/product&path=60_59&product_id=62"
                                  >
                                    <button className="w-100">view</button>
                                  </a>
                                </div>
                              </div>
                           
                </div>
            </div>
            </section>
            <section className="container-fluid ">
          <div className="row footer">
            <div className="col-sm-4 mt-2">
              <div className="col text-center text-md-left">
                <img
                  className="footer-logo"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163568/OLLORUN_IMAGES/images/elements/logo01_ollorun_ovoawo.png"
                  alt=""
                />
              </div>
              <div className="footer-social text-center text-md-left">
                <a href="https://discord.gg/k7BaPZVKjy" target="_blank">
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163584/OLLORUN_IMAGES/images/elements/social/discord_czzaml.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.instagram.com/ollorun_health/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605208415/OLLORUN_IMAGES/images/elements/social/instagram_health_g3rnti.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.instagram.com/ollorun_wealth/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605208564/OLLORUN_IMAGES/images/elements/social/instagram_wealth_lu9qpo.svg"
                    alt=""
                  />
                </a>
                <a href="https://twitter.com/Ollorun1?s=09" target="_blank">
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163586/OLLORUN_IMAGES/images/elements/social/twitter_qjpxf3.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.facebook.com/Ollorun-Network-112311330515922/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163585/OLLORUN_IMAGES/images/elements/social/facebook_qhn2co.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.youtube.com/channel/UCKM2_TtWzVmLueKhdrSbEGQ"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605207157/OLLORUN_IMAGES/images/elements/social/youtube_jz4mah.svg"
                    alt=""
                  />
                </a>
              </div>
            </div>

            <div className="col-md-3 col-sm-6 mt-2">
              <ul>
                <li>
                  <a  href="/#pannel-0">
                    {this.state.ln[121]}
                  </a>
                </li>
                <li>
                  <a  href="/#pannel-1">
                    {this.state.ln[122]}
                  </a>
                </li>
                <li>
                  <a  href="/#pannel-2">
                    {this.state.ln[123]}
                  </a>
                </li>
                <li>
                  <a to="members" smooth={true} duration={1000} href="#">
                    {this.state.ln[124]}
                  </a>
                </li>
                <li>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/store/index.php?route=common/home"
                  >
                    {this.state.ln[125]}
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-md-3 col-sm-6 mt-2">
              <ul>
                <li>
                  <a
                    href="https://store.ollorun.com/store/index.php?route=information/information&information_id=5"
                    target="_blank"
                  >
                    {this.state.ln[126]}
                  </a>
                </li>
                <li>
                  <a
                    href="https://store.ollorun.com/store/index.php?route=information/information&information_id=3"
                    target="_blank"
                  >
                    {this.state.ln[127]}{" "}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[128]}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[129]}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[130]}
                  </a>
                </li>
              </ul>
            </div>

            <div className="row  text-md-left">
              <div className="col-sm-12 mt-4">
                <p>{this.state.ln[131]}</p>
                <p>{this.state.ln[132]}</p>
                <p>
                  <img src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163565/OLLORUN_IMAGES/images/elements/icon03_ollorun_tqiiju.svg" />
                  {this.state.ln[133]}
                </p>
              </div>
            </div>
          </div>
        </section>

            </div>
        );
    }
}
export default Wpackages;