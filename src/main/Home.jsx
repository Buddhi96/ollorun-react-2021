import React from "react";
import Cookie_Accept from "./cookie_accept_panel";
import AOS from "aos";
import { Link } from "react-scroll";
import en from "../lang/en";
import fr from "../lang/fr";
import esp from "../lang/esp";
import Cookies from "universal-cookie";

const cookies = new Cookies();

const nextYear = new Date();
nextYear.setFullYear(new Date().getFullYear() + 10);
class Home extends React.Component {
  constructor(state) {
    super(state);
    this.slideritems = document.getElementsByClassName("slider-item");
    this.slideractive=0;
    if (cookies.get("Olloruncookie") === "MS") {
      this.state = {
     
        ln: en,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            UK{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
              alt=""
            />
          </a>
        ),
        stvalue: "",
        sivalue: "",
        prvalue: "",
        plvalue: "",
        cookieAccept: " ",
      };
    } else {
      this.state = {
      
        ln: en,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            UK{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
              alt=""
            />
          </a>
        ),
        stvalue: "",
        sivalue: "",
        prvalue: "",
        plvalue: "",
        cookieAccept: <Cookie_Accept set={this.setcookie} />,
      };
    }
  }
  changelang(lan) {
    if (lan === "en") {
      this.setState({
        ln: en,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            UK{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
              alt=""
            />
          </a>
        ),
      });
    
    } else if (lan === "fr") {
      this.setState({
        ln: fr,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            FR{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/FR_d2vo8g.svg"
              alt=""
            />
          </a>
        ),
      });
     
    } else if (lan === "esp") {
      this.setState({
        ln: esp,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            ESP{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163562/OLLORUN_IMAGES/images/elements/ESP_ukit8i.svg"
              alt=""
            />
          </a>
        ),
      });
    
    }
  }
  setcookie = () => {
    this.setState({ cookieAccept: " " });
    cookies.set("Olloruncookie", "MS", { expires: nextYear });
  };
  selectpack(pack) {
    if ("STANDARD" === pack) {
      this.setState({
        stvalue: "#9013FE",
        sivalue: "",
        prvalue: "",
        plvalue: "",
      });
    } else if ("SILVER" === pack) {
      this.setState({
        sivalue: "#9013FE",
        stvalue: "",
        prvalue: "",
        plvalue: "",
      });
    } else if ("PREMIUM" === pack) {
      this.setState({
        prvalue: "#9013FE",
        sivalue: "",
        stvalue: "",
        plvalue: "",
      });
    } else if ("PLATINUM" === pack) {
      this.setState({
        plvalue: "#9013FE",
        sivalue: "",
        prvalue: "",
        stvalue: "",
      });
    }
  }
  
 next(){
      this.slideritems = document.getElementsByClassName("slider-item");
 for (let index = 0; index < this.slideritems.length; index++) {
   const element = this.slideritems[index];
   element.classList="col-sm-12 p-0 m-0 slider-item d-none";
 }

  if(this.slideractive==0){
    this.slideritems[0].classList="col-sm-12 p-0 m-0 slider-item inactive-f";
    this.slideritems[6].classList="col-sm-12 p-0 m-0 slider-item active-f";
    this.slideractive=6;
  
  }else{
    this.slideritems[this.slideractive].classList="col-sm-12 p-0 m-0 slider-item inactive-f";
    this.slideritems[this.slideractive-1].classList="col-sm-12 p-0 m-0 slider-item active-f";
    this.slideractive=this.slideractive-1;
  }
  this.slideritems = document.getElementsByClassName("slider-dot");
  for (let index = 0; index < this.slideritems.length; index++) {
    const element = this.slideritems[index];
    element.classList="slider-dot";
  }
  document.getElementById("slider_dot_"+this.slideractive).classList="slider-dot active";

 }
 prev(){

  this.slideritems = document.getElementsByClassName("slider-item");
 for (let index = 0; index < this.slideritems.length; index++) {
   const element = this.slideritems[index];
   element.classList="col-sm-12 p-0 m-0 slider-item d-none";
 }
  
  if(this.slideractive==6){
    this.slideritems[6].classList="col-sm-12 p-0 m-0 slider-item inactive-b";
    this.slideritems[0].classList="col-sm-12 p-0 m-0 slider-item active-b";
    this.slideractive=0;
  }else{
    this.slideritems[this.slideractive].classList="col-sm-12 p-0 m-0 slider-item inactive-b";
    this.slideritems[this.slideractive+1].classList="col-sm-12 p-0 m-0 slider-item active-b";
    this.slideractive=this.slideractive+1;
  }
  this.slideritems = document.getElementsByClassName("slider-dot");
  
  for (let index = 0; index < this.slideritems.length; index++) {
    const element = this.slideritems[index];
    
    element.classList="slider-dot";
  }
  document.getElementById("slider_dot_"+this.slideractive).classList="slider-dot active";
  

}

  render() {
    return (
      <div>
         <nav className="navbar navbar-expand-lg fixed-top">
          <a className="navbar-brand">
            <img
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_150,f_auto,q_auto/v1598163568/OLLORUN_IMAGES/images/elements/logo01_ollorun_ovoawo.png"
              alt=""
            />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span>
              <img
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/ham_y9sttw.svg"
                alt=""
              />
            </span>
          </button>
          <div
            className="collapse navbar-collapse justify-content-end"
            id="navbarNav"
          >
            <ul className="navbar-nav">
              <li
                className="nav-item active"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <Link
                  to="pannel-0"
                  smooth={"true"}
                  duration={1000}
                  className="nav-link"
                  href="#"
                >
                  {this.state.ln[0]} <span className="sr-only">(current)</span>
                </Link>
              </li>
              <li
                className="nav-item"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <Link
                  to="pannel-1"
                  smooth={"true"}
                  duration={1000}
                  className="nav-link"
                  href="#"
                >
                  {this.state.ln[1]}
                </Link>
              </li>
              <li
                className="nav-item"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <Link
                  to="pannel-2"
                  smooth={"true"}
                  duration={1000}
                  className="nav-link"
                  href="#"
                >
                  {this.state.ln[2]}
                </Link>
              </li>
              <li
                className="nav-item"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  target="_blank"
                  smooth={"true"}
                  duration={1000}
                  className="nav-link"
                  href={this.state.ln[151]}
                >
                  {this.state.ln[150]}
                </a>
              </li>
              <li
                className="nav-item text-center"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  className="nav-link"
                  target="_BLANK"
                  href="https://training.ollorun.com/index"
                >
                  TRAINING
                </a>
              </li>
              <li
                className="nav-item text-center"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  className="nav-link join-btn"
                  target="_BLANK"
                  href="https://backoffice.ollorun.com/"
                >
                  {this.state.ln[3]}
                </a>
              </li>
              <li className="nav-item dropdown">
                {this.state.flag}
                <div
                  className="dropdown-menu"
                  aria-labelledby="navbarDropdown"
                  data-toggle="collapse"
                  data-target="#navbarNav"
                >
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("en")}
                  >
                    UK{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
                      alt=""
                    />
                  </a>
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("fr")}
                  >
                    FR{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/FR_d2vo8g.svg"
                      alt=""
                    />
                  </a>
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("esp")}
                  >
                    ESP{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163562/OLLORUN_IMAGES/images/elements/ESP_ukit8i.svg"
                      alt=""
                    />
                  </a>
                </div>
              </li>
            </ul>
          </div>
        </nav>
        <section
         
          id="pannel-0"
          className="top view-pannel vp-active"
        >
          <div className="video-overlay-1"></div>
          <video autoPlay muted>
            <source
              src="https://res.cloudinary.com/dmeppscpx/video/upload/v1617739400/original_cxxffg.mp4"
              type="video/mp4"
            />
          </video>
          <div className="video-overlay-2"></div>
          <div className="content my-auto">
            <div className="logo">
              <img
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_700,f_auto,q_auto/v1598163569/OLLORUN_IMAGES/images/elements/logo02_ollorun_df7xdq.png"
                alt=""
              />
            </div>
            <div>
              <h2>{this.state.ln[4]}</h2>
            </div>
            <div className="explore">
              <div>
                <a href="#pannel-1" className="explore-btn">
                  explore
                </a>
              </div>
            </div>
          </div>
        </section>
        <section id="pannel-1" className="pannel-1">
          <div className="container-fluid p-0 h-100" >
            <div className="col-sm-12 d-flex p-0 h-100">
              <div className="col-sm-6 h-100 w-side " data-aos="fade-up" data-aos-duration="3000">
                <div className="row h-100">
                  <div className="hw-content my-auto">
                   
                      <div className="img-div">
                        <img
                          className="w-img"
                          src="https://res.cloudinary.com/sapiangroup/image/upload/w_600,f_auto,q_auto/v1598163570/OLLORUN_IMAGES/images/elements/LogoW_Gold_ollorun_nqexrt.png"
                          alt=""
                        />
                      </div>
                  
                  <h2>ENHANCE WEALTH</h2>
                  <a
                      href="/welth-packs"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                  <div className="m-auto col-sm-3  btn-div">EXPLORE</div>
                  </a>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 h-100 h-side" data-aos="fade-up" data-aos-duration="3000">
                <div className="row h-100">
                  <div className="hw-content my-auto">
                    
                      <div className="img-div">
                        <img
                          className="h-img"
                          src="https://res.cloudinary.com/sapiangroup/image/upload/w_600,f_auto,q_auto/v1598163569/OLLORUN_IMAGES/images/elements/LogoH_Blue_ollorun_d1xvzi.png"
                          alt=""
                        />
                      </div>
                    
                    <h2>ENHANCE HEALTH</h2>
                    <a
                      href="/helth-packs"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                    <div className=" m-auto col-sm-3 btn-div">EXPLORE</div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="pannel-2" className="two pannel-2">
          <div className="container-fluid h-100">
            <div className="row h-100">
              <div className="col-sm-4 side-img_div">
                <img 
                  src="https://res.cloudinary.com/buddhidev/image/upload/v1624264498/Ollorun/new_opportunity-min_uf34zc.png"
                  alt=""
                  srcSet=""
                />
              </div>
              <div className="col-sm-1"></div>
              <div className="col-sm-6 m-auto">
                <div className="row mt-5">
                  <h4>{this.state.ln[2]}</h4>
                </div>
                <div className="row mt-4 ">
                  <p data-aos="fade-up" data-aos-duration="800">
                    {this.state.ln[22]}{" "}
                  </p>
                </div>
              </div>
              <div className="col-sm-1"></div>
            </div>
          </div>
        </section>
        <section id="pannel-3" className="pannel-3 view-pannel">
          <div className="container pt-5 h-100">
            <div className="row h-100">
              <div className="col-sm-8 m-auto">
                <div data-aos="fade-up" data-aos-duration="800">
                  <h5>{this.state.ln[23]}</h5>
                  <h4>{this.state.ln[24]}</h4>
                </div>

                <div className="content-right mt-5">
                  <p data-aos="fade-up" data-aos-duration="800">
                    {this.state.ln[25]}
                  </p>
                  <p
                    data-aos="fade-up"
                    data-aos-duration="800"
                    data-aos-delay="200"
                  >
                    {this.state.ln[26]}{" "}
                  </p>
                  <p
                    data-aos="fade-up"
                    data-aos-duration="800"
                    data-aos-delay="400"
                  >
                    {this.state.ln[27]}{" "}
                  </p>
                </div>
              </div>
              <div className="content-left col-sm-4 text-center mt-5 p-0">
                {/* <p>{this.state.ln[28]}</p> */}

                <div className="pannel-button d-flex" data-aos="fade-left">
                  <img
                    src="https://res.cloudinary.com/buddhidev/image/upload/v1624267208/Ollorun/blockchain_gybbdr_awgblu.svg"
                    alt=""
                  />
                  <p> {this.state.ln[29]}</p>
                </div>
                <div
                  className="pannel-button d-flex"
                  data-aos="fade-left"
                  data-aos-delay="50"
                >
                  <img
                    src="https://res.cloudinary.com/buddhidev/image/upload/v1624267208/Ollorun/bio_fccuao_cufizf.svg"
                    alt=""
                  />
                  <p>{this.state.ln[30]}</p>
                </div>
                <div
                  className="pannel-button d-flex"
                  data-aos="fade-left"
                  data-aos-delay="100"
                >
                  <img
                    src="https://res.cloudinary.com/buddhidev/image/upload/v1624267208/Ollorun/work_wwkwov_qwscrk.svg"
                    alt=""
                  />
                  <p>{this.state.ln[31]}</p>
                </div>
                <div
                  className="pannel-button d-flex "
                  data-aos="fade-left"
                  data-aos-delay="150"
                >
                  <img
                    src="https://res.cloudinary.com/buddhidev/image/upload/v1624267208/Ollorun/internet_dusaoi_iu5att.svg"
                    alt=""
                  />
                  <p>{this.state.ln[32]}</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="pannel-4" className="pannel-4 view-pannel mt-5">
        <div className="container-fluid   mobile-back"></div>
          <div className="container-fluid mt-5 h-100">
            <div className="row mt-5 h-100">
              <div className="col-sm-4 side">
                <img
                  className="my-auto"
                  src="https://res.cloudinary.com/buddhidev/image/upload/v1624343611/Ollorun/why_one_shadow_bolbvz.png"
                  alt=""
                  srcSet=""
                />
              </div>
              <div className="col-sm-1"></div>
              <div className=" col-sm-7 why-headings mt-5">
                <h4 data-aos="fade-down" data-aos-duration="1200">
                  {this.state.ln[50]}
                </h4>
                <h5
                  data-aos="fade-up"
                  data-aos-duration="1200"
                  data-aos-delay="800"
                >
                  {this.state.ln[51]}
                </h5>
                <div className="row">
                <div className="col-sm-6 mt-3">
                  <div
                    className="universe w-div"
                    data-aos="fade-right"
                    data-aos-duration="1200"
                    data-aos-delay="800"
                  >
                    {this.state.ln[52]}
                  </div>
                </div>
                <div className="col-sm-6 mt-3">
                  <div
                    className="universe h-div"
                    data-aos="fade-left"
                    data-aos-duration="1200"
                    data-aos-delay="800"
                  >
                    {this.state.ln[53]}
                  </div>
                </div>
                
                </div>
               <div className="row mt-5">
                  <div className="col-sm-7"></div>
                  <div className=" col-sm-12 why-headings">
                    <h4
                      data-aos="fade-down"
                      data-aos-duration="800"
                      data-aos-delay="1000"
                    >
                      {this.state.ln[54]}
                    </h4>
                    <h5
                      data-aos="fade-up"
                      data-aos-duration="800"
                      data-aos-delay="1200"
                    >
                      {this.state.ln[55]}
                    </h5>
                  </div>
                </div>

                <div className="row mt-5">
                  <div className="col-sm-6 mt-3">
                    <div
                      className="universe"
                      data-aos="fade-up"
                      data-aos-duration="800"
                      data-aos-delay="800"
                    >
                      <p className="my-auto">{this.state.ln[56]}</p>
                    </div>
                  </div>
                  <div className="col-sm-6 mt-3">
                    <div
                      className="universe"
                      data-aos="fade-up"
                      data-aos-duration="800"
                      data-aos-delay="800"
                    >
                      <p className="my-auto">{this.state.ln[57]}</p>
                    </div>
                  </div>
                  <div className="col-sm-6 mt-3">
                    <div
                      className="universe"
                      data-aos="fade-up"
                      data-aos-duration="800"
                      data-aos-delay="1000"
                    >
                      <p className="my-auto"> {this.state.ln[58]}</p>
                    </div>
                  </div>
                  <div className="col-sm-6 mt-3">
                    <div
                      className="universe"
                      data-aos="fade-up"
                      data-aos-duration="800"
                      data-aos-delay="1000"
                    >
                      <p className="my-auto">{this.state.ln[59]}</p>
                    </div>
                  </div>
                </div>
                <div className="row mt-5">
                  <div className="col">
                    
                      <a
                        target="_blank"
                        href="https://store.ollorun.com/index.php?route=product/category&path=62"
                      >
                        <div className="m-auto col-sm-3 mb-5 btn-div kit">
                        {this.state.ln[49]}: 29€
                        </div>
                        
                      </a>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="pannel-5" className="pannel-5 pb-5 view-pannel">
          <div className="container h-100 pt-4 pb-5">
          <div className="row heding">
              <h5 className="topic">{this.state.ln[72]}</h5>
              <h4 className="mb-4">{this.state.ln[73]}</h4>
            </div>
         <div data-aos="fade-up"
     data-aos-anchor-placement="center-bottom">
          <div className="col-sm-12 mt-5 d-flex p-0  content justify-content-center">
            <div id="slider_item_1" className="col-sm-12 p-0 m-0 slider-item active-f">
              <div  className="col-sm-7 slider-item_content">
              <div className="text">
            <h6>{this.state.ln[74]}</h6>
            <p>
           {this.state.ln[75]}
            </p>
        </div>
              </div>
              <div className="col-sm-5 slider-item_side "> 
              <img className="d-block w-75 my-auto " src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163576/OLLORUN_IMAGES/images/elements/slides-img/1_kchuzj.svg" alt="First slide" />
       
              </div>
            </div>
            <div id="slider_item_2" className="col-sm-12 p-0 m-0 slider-item inactive-f d-none">
            <div className="col-sm-5 slider-item_side_l ">
            <img className="d-block w-75  my-auto" src="https://res.cloudinary.com/sapiangroup/image/upload/f_auto,q_auto/v1598163582/OLLORUN_IMAGES/images/elements/slides-img/2_wylhsr.svg"
            alt="Second slide" />
               </div>
            <div className="col-sm-7 slider-item_content">
            <div className="text">
            <h6>{this.state.ln[76]}</h6>
            <p>
            {this.state.ln[77]}</p>
        </div>
            </div>
            <div className="col-sm-5 slider-item_side_r ">
            <img className="d-block w-75 my-auto" src="https://res.cloudinary.com/sapiangroup/image/upload/f_auto,q_auto/v1598163582/OLLORUN_IMAGES/images/elements/slides-img/2_wylhsr.svg"
            alt="Second slide" />
               </div>
            </div>
            <div id="slider_item_3" className="col-sm-12 p-0 m-0 slider-item d-none">
            <div className="col-sm-7 slider-item_content">
            <div className="text">
            <h6>{this.state.ln[78]}</h6>
            <p>{this.state.ln[79]}</p>
        </div>
            </div>
            <div className="col-sm-5 slider-item_side ">
            <img className="d-block w-75 my-auto" src={this.state.ln[149]}
            alt="Third slide" /> </div>
            </div>
            <div id="slider_item_4" className="col-sm-12 p-0 m-0 slider-item d-none">
            <div className="col-sm-5 slider-item_side_l ">
            <img className="d-block w-75 my-auto" src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163580/OLLORUN_IMAGES/images/elements/slides-img/4_weow42.svg"
            alt="Third slide" />
               </div>
            <div className="col-sm-7 slider-item_content">
              <div className="text">
              <h6>{this.state.ln[80]}</h6>
            <p>{this.state.ln[81]}</p>
        </div>
        </div>
        <div className="col-sm-5 slider-item_side_r  ">
            <img className="d-block w-75 my-auto" src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163580/OLLORUN_IMAGES/images/elements/slides-img/4_weow42.svg"
            alt="Third slide" />
               </div>
            </div>
            <div id="slider_item_5" className="col-sm-12 p-0 m-0 slider-item d-none">
            <div className="col-sm-7 slider-item_content">
            <div className="text">
            <h6>{this.state.ln[82]}</h6>
            <p>{this.state.ln[83]}</p>
        </div>
            </div>
            <div className="col-sm-5 slider-item_side ">
            <img className="d-block w-75 my-auto" src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163582/OLLORUN_IMAGES/images/elements/slides-img/5_okeelt.svg"
            alt="Third slide" />
               </div>
            </div>
            <div id="slider_item_6" className="col-sm-12 p-0 m-0 slider-item d-none">
            <div className="col-sm-5 slider-item_side_l ">
            <img className="d-block w-75 my-auto" src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163584/OLLORUN_IMAGES/images/elements/slides-img/6_zwicum.svg"
            alt="Third slide" />
             </div>
            <div className="col-sm-7 slider-item_content">
              <div className="text">
              <h6>{this.state.ln[84]}</h6>
            <p>{this.state.ln[85]}</p>
        </div>
        </div>
        <div className="col-sm-5 slider-item_side_r  ">
            <img className="d-block w-75 my-auto" src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163584/OLLORUN_IMAGES/images/elements/slides-img/6_zwicum.svg"
            alt="Third slide" />
             </div>
            </div>
            <div id="slider_item_7" className="col-sm-12 p-0 m-0 slider-item inactive-b d-none">
            <div className="col-sm-7 slider-item_content">
            <div className="text">
            <h6>{this.state.ln[86]}</h6>
            <p>{this.state.ln[87]}</p>
        </div>
            </div>
            <div className="col-sm-5 slider-item_side "> 
            <img className="d-block w-75 my-auto" src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163583/OLLORUN_IMAGES/images/elements/slides-img/7_dea9hl.svg"
            alt="Third slide" />
            </div>
            </div>
            <div className="col-sm-12 d-flex justify-content-center m-0 btn-row">
            <div onClick={() => this.next()} className="nav-btn col-sm-1 btn-l">&lt;</div>
            <div className="col-sm-11 "></div>
           <div onClick={() => this.prev()} className="nav-btn col-sm-1 btn-r">&gt;</div>
            </div>
            </div>
            <div className="col-sm-12 d-flex mt-3 slider-indicaters justify-content-center">
              <span id="slider_dot_0"  className="slider-dot active"></span>
              <span id="slider_dot_1" className="slider-dot"></span>
              <span id="slider_dot_2" className="slider-dot"></span>
              <span id="slider_dot_3" className="slider-dot"></span>
              <span id="slider_dot_4" className="slider-dot"></span>
              <span id="slider_dot_5" className="slider-dot"></span>
              <span id="slider_dot_6" className="slider-dot"></span>
            </div>
            </div>
          </div>
        </section>
        <section id="pannel-6" className="pannel-6 view-pannel">
          <div className="container-fluid h-100">
            <div className="row h-100">
              <div className="col-sm-7 m-auto">
                
                <div className="network-heading">
                  <h4>{this.state.ln[88]}</h4>
                  <h5>{this.state.ln[89]}</h5>
                </div>
                <div className="col-sm-12 three_items mb-5">
                  <div className="row mt-5 h-100">
                    <div className="col-sm mt-2 universe " data-aos="fade-up" data-aos-duration="600">
                      {this.state.ln[90]}
                    </div>
                    <div className="col-sm mt-2 universe " data-aos="fade-up" data-aos-duration="800" >
                     {this.state.ln[91]}
                    </div>
                    <div
                      className="col-sm mt-2 universe "
                      data-aos="fade-up"
                      data-aos-duration="1000"
                    >
                      {this.state.ln[92]}
                    </div>
                  </div>
                </div>
              <div className="col-sm-12 mt-5">
                    <div className="social">
                      <div className="mb-4 text">{this.state.ln[93]}</div>
                      <div className="icons">
                      <a target="_blank" href="https://store.ollorun.com/">
                        <img
                          src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163587/OLLORUN_IMAGES/images/elements/website_ollorun_aln6mu.svg"
                          alt="ollorun store"
                        />
                      </a>
                      <a target="_blank" href="https://discord.gg/k7BaPZVKjy">
                        <img
                          src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163559/OLLORUN_IMAGES/images/elements/Discord_ollorun_lo34rw.svg"
                          alt="ollorun discord server"
                        />
                      </a>
                      <a
                        target="_blank"
                        href="https://twitter.com/Ollorun1?s=09"
                      >
                        <img
                          src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163587/OLLORUN_IMAGES/images/elements/twitter_ollorun_ewokii.svg"
                          alt="ollorun twitter"
                        />
                      </a>
                      <a
                        target="_blank"
                        href="https://www.facebook.com/Ollorun-Network-112311330515922/"
                      >
                        <img
                          src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163562/OLLORUN_IMAGES/images/elements/facebook_ollorun_twllda.svg"
                          alt="ollorun facebook"
                        />
                      </a>
                      <a
                        target="_blank"
                        href="https://www.instagram.com/ollorun_health/"
                      >
                        <img
                          src="https://res.cloudinary.com/dmeppscpx/image/upload/v1618988105/ollorun-socials/SVG/s-health_ibgu38.svg"
                          alt="ollorun health instagram"
                        />
                      </a>
                      <a
                        target="_blank"
                        href="https://www.instagram.com/ollorun_wealth/"
                      >
                        <img
                          src="https://res.cloudinary.com/dmeppscpx/image/upload/v1618988105/ollorun-socials/SVG/s-wealth_ckim7s.svg"
                          alt="ollorun wealth instagram"
                        />
                      </a>
                   </div>
                      </div>
                
                </div>
             
                
                </div>
             <div className="col-sm-1"></div>
              <div className="col-sm-4 side">
                <img src="https://res.cloudinary.com/buddhidev/image/upload/v1624428715/Ollorun/network_one-shadow_zqi4nf.png" alt="" srcSet="" />
              </div>
            </div>
          </div>
        </section>
        <section id="pannel-8" className="sec-change view-pannel">
          <section className="container main-sec">
            <div className="row sec-3 d-flex justify-content-center">
              <div className="col-md-12">
                <h5>{this.state.ln[8]}</h5>
              </div>
              <div className="col-sm-12 mt-4 range-wrap">
                <h4 className="text-center text-md-left">
                  {this.state.ln[148]}
                </h4>
              </div>
              <div
                data-aos="fade-up"
                className="col-6 col-xl-3 col-sm-6 aos-init aos-animate"
              >
                <div className="package">
                  <div className="row">
                    <input type="hidden" className="number" value="126" />
                    <div className="col-sm-12 d-flex">
                      <div
                        className="dot"
                        style={{ background: this.state.stvalue }}
                      ></div>
                      <h6 className="wealth-package mx-auto">
                        {this.state.ln[9]}
                      </h6>
                    </div>
                    <div className="col text-center"></div>
                    <div className="col-sm-12">
                      <p className="package-name">{this.state.ln[12]}</p>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[10]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 yearprofit">630.63%</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[11]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 o-rate">0.137</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 mb-3 package-select">
                      <button
                        onClick={() => this.selectpack("STANDARD")}
                        className="select-button"
                      >
                        {this.state.ln[144]}
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div
                data-aos="fade-up"
                className="col-6 col-xl-3 col-sm-6 aos-init aos-animate"
              >
                <div className="package">
                  <div className="row">
                    <input type="hidden" className="number" value="335" />
                    <div className="col-sm-12 d-flex">
                      <div
                        className="dot"
                        style={{ background: this.state.sivalue }}
                      ></div>
                      <h6 className="wealth-package mx-auto">
                        {this.state.ln[9]}
                      </h6>
                    </div>
                    <div className="col-sm-12">
                      <p className="package-name">{this.state.ln[15]}</p>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[10]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 yearprofit">630.63%</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[11]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 o-rate">0.137</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 mb-3 package-select">
                      <button
                        onClick={() => this.selectpack("SILVER")}
                        className="select-button"
                      >
                        {this.state.ln[144]}
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div
                data-aos="fade-up"
                className="col-6 col-xl-3 col-sm-6 aos-init aos-animate"
              >
                <div className="package">
                  <div className="row">
                    <input type="hidden" className="number" value="620" />
                    <div className="col-sm-12 d-flex">
                      <div
                        className="dot"
                        style={{ background: this.state.prvalue }}
                      ></div>
                      <h6 className="wealth-package mx-auto">
                        {this.state.ln[9]}
                      </h6>
                    </div>
                    <div className="col-sm-12">
                      <p className="package-name">{this.state.ln[16]}</p>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[10]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 yearprofit">630.63%</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[11]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 o-rate">0.137</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 mb-3 package-select">
                      <button
                        onClick={() => this.selectpack("PREMIUM")}
                        className="select-button"
                      >
                        {this.state.ln[144]}
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div
                data-aos="fade-up"
                className="col-6 col-xl-3 col-sm-6 aos-init aos-animate"
              >
                <div className="package">
                  <div className="row">
                    <input type="hidden" className="number" value="1250" />
                    <div className="col-sm-12 d-flex">
                      <div
                        className="dot"
                        style={{ background: this.state.plvalue }}
                      ></div>
                      <h6 className="wealth-package mx-auto">
                        {this.state.ln[9]}
                      </h6>
                    </div>
                    <div className="col-sm-12">
                      <p className="package-name">{this.state.ln[18]}</p>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[10]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 yearprofit">630.63%</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[11]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 o-rate">0.137</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 mb-3 package-select">
                      <button
                        onClick={() => this.selectpack("PLATINUM")}
                        className="select-button"
                      >
                        {this.state.ln[144]}
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-sm-12 mt-4 range-wrap range-wrap-main">
                <h5>{this.state.ln[20]}</h5>
                <input
                  id="range"
                  name="range"
                  className="range range-slider"
                  step="0.001"
                  type="range"
                  min="0.137"
                  max="1"
                />
                <output className="bubble"></output>
              </div>

              <div className="col-sm-12 yearstats mt-5">
                <div className="row d-flex justify-content-center">
                  {/*     <!-- stat --> */}
                  <div
                    className="col-sm-6 col-md-2 col-6 stat"
                    data-aos="fade-up"
                    data-aos-delay="50"
                    data-aos-duration="800"
                  >
                    <div className="row">
                      <div className="col-sm-12 days">
                        <h6 id="days">30 {this.state.ln[21]}</h6>
                      </div>

                      <div className="col-sm-12 percentage">
                        <h3 id="percentage">73.62%</h3>
                        <p className="my-3 text-center">{this.state.ln[145]}</p>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">Reward</span>
                        <span id="reward" className="value doller-value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">EUR</span>
                        <span id="usdt" className="value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">OZTG</span>
                        <span id="oztg" className="value">
                          43.32
                        </span>
                      </div>
                    </div>
                  </div>

                  {/* <!-- stat --> */}
                  <div
                    className="col-sm-6 col-md-2 col-6 stat"
                    data-aos="fade-up"
                    data-aos-delay="100"
                    data-aos-duration="800"
                  >
                    <div className="row">
                      <div className="col-sm-12 days">
                        <h6 id="days">90 {this.state.ln[21]}</h6>
                      </div>

                      <div className="col-sm-12 percentage">
                        <h3 id="percentage">73.62%</h3>
                        <p className="my-3 text-center">{this.state.ln[145]}</p>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">Reward</span>
                        <span id="reward" className="value doller-value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">EUR</span>
                        <span id="usdt" className="value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">OZTG</span>
                        <span id="oztg" className="value">
                          43.32
                        </span>
                      </div>
                    </div>
                  </div>

                  {/* <!-- stat --> */}
                  <div
                    className="col-sm-6 col-md-2 col-6 stat"
                    data-aos="fade-up"
                    data-aos-delay="150"
                    data-aos-duration="800"
                  >
                    <div className="row">
                      <div className="col-sm-12 days">
                        <h6 id="days">180 {this.state.ln[21]}</h6>
                      </div>

                      <div className="col-sm-12 percentage">
                        <h3 id="percentage">73.62%</h3>
                        <p className="my-3 text-center">{this.state.ln[145]}</p>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">Reward</span>
                        <span id="reward" className="value doller-value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">EUR</span>
                        <span id="usdt" className="value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">OZTG</span>
                        <span id="oztg" className="value">
                          43.32
                        </span>
                      </div>
                    </div>
                  </div>

                  {/*  <!-- stat --> */}
                  <div
                    className="col-sm-6 col-md-2 col-6 stat"
                    data-aos="fade-up"
                    data-aos-delay="200"
                    data-aos-duration="800"
                  >
                    <div className="row">
                      <div className="col-sm-12 days">
                        <h6 id="days">360 {this.state.ln[21]}</h6>
                      </div>

                      <div className="col-sm-12 percentage">
                        <h3 id="percentage">73.62%</h3>
                        <p className="my-3 text-center">{this.state.ln[145]}</p>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">Reward</span>
                        <span id="reward" className="value doller-value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">EUR</span>
                        <span id="usdt" className="value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">OZTG</span>
                        <span id="oztg" className="value">
                          43.32
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          {/*  <!-- packs --> */}
        </section>

        <section id="pannel-9" className="last-carousel view-pannel">
          <video
            src="https://res.cloudinary.com/dmeppscpx/video/upload/v1619114849/1297185095_zqc8h3.mp4"
            className="reward-image"
            loop
            autoPlay
            muted
          ></video>
          <div
            className="reward-image"
            style={{
              backgroundImage:
                "url(https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_900/v1619115165/supercar_eraydk.jpg)",
            }}
          ></div>
          <div
            className="reward-image"
            style={{
              backgroundImage:
                "url(https://res.cloudinary.com/sapiangroup/image/upload/w_1900,f_auto,q_auto/v1598163556/OLLORUN_IMAGES/images/rewards/Gold_ollorun_oxzwfg.jpg)",
            }}
          ></div>
          <div
            className="reward-image"
            style={{
              backgroundImage:
                "url(https://res.cloudinary.com/sapiangroup/image/upload/w_900,f_auto,q_auto/v1598163558/OLLORUN_IMAGES/images/rewards/House_ollorun_fzimcp.jpg)",
            }}
          ></div>
          <div
            className="reward-image"
            style={{
              backgroundImage:
                "url(https://res.cloudinary.com/sapiangroup/image/upload/w_900,f_auto,q_auto/v1598163556/OLLORUN_IMAGES/images/rewards/Boat_ollorun_yfow8w.jpg)",
            }}
          ></div>
          <div
            className="reward-image"
            style={{
              backgroundImage:
                "url(https://res.cloudinary.com/dmeppscpx/image/upload/v1619350899/ip_bjm9oz.png)",
            }}
          ></div>
          <div
            className="reward-image"
            style={{
              backgroundImage:
                "url(https://res.cloudinary.com/dmeppscpx/image/upload/v1619349635/mac_p5to8s.png)",
            }}
          ></div>
          <div
            className="reward-image"
            style={{
              backgroundImage:
                "url(https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,q_auto,w_1200/v1619115951/tesla_ee1mzy.jpg)",
            }}
          ></div>

          {/* 1 */}
          <div className="reward-info">
            <div className="reward-info-area">
              <div>
                <img
                  className="reward-badge"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163555/OLLORUN_IMAGES/images/elements/Diamond_ollorun_xsbiic.png"
                  alt=""
                />
              </div>

              <div className="reward-value">96 000</div>
              <div className="reward-name">{this.state.ln[106]}</div>
              <div className="reward-desc">{this.state.ln[102]}</div>
              <div>
                <ul>
                  <li>{this.state.ln[103]}</li>
                  <li>{this.state.ln[104]}</li>
                  <li>{this.state.ln[105]}</li>
                </ul>
              </div>
            </div>
          </div>
          {/* 2 */}
          <div className="reward-info">
            <div className="reward-info-area">
              <div>
                <img
                  className="reward-badge"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163549/OLLORUN_IMAGES/images/elements/Blue_Diamond_ollorun_hsu5li.png"
                  alt=""
                />
              </div>

              <div className="reward-value">384 000</div>
              <div className="reward-name">{this.state.ln[108]}</div>
              <div className="reward-desc">{this.state.ln[107]}</div>
              <div>
                <ul>
                  <li>{this.state.ln[105]}</li>
                </ul>
                <div>{this.state.ln[100]}</div>
              </div>
            </div>
          </div>
          {/* 3 */}
          <div className="reward-info">
            <div className="reward-info-area">
              <div>
                <img
                  className="reward-badge"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163544/OLLORUN_IMAGES/images/elements/Black_Diamond_ollorun_vyop4v.png"
                  alt=""
                />
              </div>

              <div className="reward-value">1 536 000</div>
              <div className="reward-name">{this.state.ln[110]}</div>
              <div className="reward-desc">{this.state.ln[109]}</div>
              <div>
                <ul>
                  <li>{this.state.ln[105]}</li>
                </ul>
              </div>
            </div>
          </div>

          {/* 4 */}
          <div className="reward-info">
            <div className="reward-info-area">
              <div>
                <img
                  className="reward-badge"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163558/OLLORUN_IMAGES/images/elements/Crown_Diamond_ollorun_yyzmr4.png"
                  alt=""
                />
              </div>

              <div className="reward-value">6 144 000</div>
              <div className="reward-name">{this.state.ln[113]}</div>
              <div className="reward-desc">{this.state.ln[111]}</div>
              <div>
                <ul>
                  <li>{this.state.ln[105]}</li>
                  <li>{this.state.ln[111]}</li>
                </ul>
              </div>
            </div>
          </div>

          {/* 5 */}
          <div className="reward-info">
            <div className="reward-info-area">
              <div>
                <img
                  className="reward-badge"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163552/OLLORUN_IMAGES/images/elements/Crown_Ambassador_ollorun_bfgknq.png"
                  alt=""
                />
              </div>

              <div className="reward-value">24 576 000</div>
              <div className="reward-name">{this.state.ln[118]}</div>
              <div className="reward-desc">{this.state.ln[114]}</div>
              <div>
                <ul>
                  <li>{this.state.ln[115]}</li>
                  <li>{this.state.ln[116]}</li>
                  <li>{this.state.ln[117]}</li>
                  <li>{this.state.ln[110]}</li>
                </ul>
              </div>
            </div>
          </div>

          {/* 6 */}
          <div className="reward-info">
            <div className="reward-info-area">
              <div>
                <img
                  className="reward-badge"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163584/OLLORUN_IMAGES/images/elements/Sapphire_ollorun_mehkcg.png"
                  alt=""
                />
              </div>

              <div className="reward-value">6000</div>
              <div className="reward-name">{this.state.ln[96]}</div>
              <div className="reward-desc">{this.state.ln[95]}</div>
            </div>
          </div>

          {/* 7 */}
          <div className="reward-info">
            <div className="reward-info-area">
              <div>
                <img
                  className="reward-badge"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163576/OLLORUN_IMAGES/images/elements/Ruby_ollorun_o4lilk.png"
                  alt=""
                />
              </div>

              <div className="reward-value">12 000</div>
              <div className="reward-name">{this.state.ln[98]}</div>
              <div className="reward-desc">{this.state.ln[97]}</div>
            </div>
          </div>

          {/* 8 */}
          <div className="reward-info">
            <div className="reward-info-area">
              <div>
                <img
                  className="reward-badge"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163560/OLLORUN_IMAGES/images/elements/Emerald_ollorun_llwsit.png"
                  alt=""
                />
              </div>

              <div className="reward-value">24 000</div>
              <div className="reward-name">{this.state.ln[101]}</div>
              <div className="reward-desc">{this.state.ln[99]}</div>
            </div>
          </div>

          <div id="next-reward" className="next-reward">
            <img
              src="https://res.cloudinary.com/dmeppscpx/image/upload/v1619020599/next_my4d5o.svg"
              alt=""
            />
          </div>
          <div id="prev-reward" className="prev-reward">
            <img
              src="https://res.cloudinary.com/dmeppscpx/image/upload/v1619020599/prev_tfvaer.svg"
              alt=""
            />
          </div>
        </section>
        <section className="ready">
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm-12">
                <span>{this.state.ln[120]}</span>
              </div>
              <div className="col-sm-12">
                <div>
                  <a
                    className="accent-btn"
                    target="_blank"
                    href="https://backoffice.ollorun.com/"
                  >
                    {this.state.ln[3]}
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="container-fluid ">
          <div className="row footer">
            <div className="col-sm-4 mt-2">
              <div className="col text-center text-md-left">
                <img
                  className="footer-logo"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163568/OLLORUN_IMAGES/images/elements/logo01_ollorun_ovoawo.png"
                  alt=""
                />
              </div>
              <div className="footer-social text-center text-md-left">
                <a href="https://discord.gg/k7BaPZVKjy" target="_blank">
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163584/OLLORUN_IMAGES/images/elements/social/discord_czzaml.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.instagram.com/ollorun_health/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605208415/OLLORUN_IMAGES/images/elements/social/instagram_health_g3rnti.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.instagram.com/ollorun_wealth/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605208564/OLLORUN_IMAGES/images/elements/social/instagram_wealth_lu9qpo.svg"
                    alt=""
                  />
                </a>
                <a href="https://twitter.com/Ollorun1?s=09" target="_blank">
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163586/OLLORUN_IMAGES/images/elements/social/twitter_qjpxf3.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.facebook.com/Ollorun-Network-112311330515922/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163585/OLLORUN_IMAGES/images/elements/social/facebook_qhn2co.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.youtube.com/channel/UCKM2_TtWzVmLueKhdrSbEGQ"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605207157/OLLORUN_IMAGES/images/elements/social/youtube_jz4mah.svg"
                    alt=""
                  />
                </a>
              </div>
            </div>

            <div className="col-md-3 col-sm-6 mt-2">
              <ul>
                <li>
                  <Link to="top" smooth={"true"} duration={1000} href="#">
                    {this.state.ln[121]}
                  </Link>
                </li>
                <li>
                  <Link to="products" smooth={"true"} duration={1000} href="#">
                    {this.state.ln[122]}
                  </Link>
                </li>
                <li>
                  <Link to="opportunity" smooth={"true"} duration={1000} href="#">
                    {this.state.ln[123]}
                  </Link>
                </li>
                <li>
                  <Link to="members" smooth={"true"} duration={1000} href="#">
                    {this.state.ln[124]}
                  </Link>
                </li>
                <li>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/store/index.php?route=common/home"
                  >
                    {this.state.ln[125]}
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-md-3 col-sm-6 mt-2">
              <ul>
                <li>
                  <a
                    href="https://store.ollorun.com/store/index.php?route=information/information&information_id=5"
                    target="_blank"
                  >
                    {this.state.ln[126]}
                  </a>
                </li>
                <li>
                  <a
                    href="https://store.ollorun.com/store/index.php?route=information/information&information_id=3"
                    target="_blank"
                  >
                    {this.state.ln[127]}{" "}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[128]}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[129]}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[130]}
                  </a>
                </li>
              </ul>
            </div>

            <div className="row  text-md-left">
              <div className="col-sm-12 mt-4">
                <p>{this.state.ln[131]}</p>
                <p>{this.state.ln[132]}</p>
                <p>
                  <img src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163565/OLLORUN_IMAGES/images/elements/icon03_ollorun_tqiiju.svg" />
                  {this.state.ln[133]}
                </p>
              </div>
            </div>
          </div>
        </section>

        {this.state.cookieAccept}
      </div>
    );
  }
  componentDidMount() {
    AOS.init();

    const allRanges = document.querySelectorAll(".range-wrap-main");
    allRanges.forEach((wrap) => {
      const range = wrap.querySelector(".range");
      const bubble = wrap.querySelector(".bubble");
      range.value = 0;
      range.style.animation = "gradient 1s ease infinite";

      range.addEventListener("input", () => {
        setBubble(range, bubble);
      });

      setBubble(range, bubble);
    });

    function setBubble(range, bubble) {
      const val = range.value;
      const min = range.min ? range.min : 0;
      const max = range.max ? range.max : 100;
      const newVal = Number(((val - min) * 100) / (max - min));
      bubble.innerHTML = "$ " + val;

      const color =
        "linear-gradient(90deg, #360066 0%, var(--main-color) " +
        newVal +
        "%, var(--main-light) " +
        newVal +
        "%)";
      range.style.background = color;

      // Sorta magic numbers based on size of the native UI thumb
      bubble.style.left = `calc(${newVal}% + (${8 - newVal * 0.15}px))`;
    }
  }
}

export default Home;
