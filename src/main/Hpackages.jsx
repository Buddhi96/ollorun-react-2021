import React from "react";
import en from "../lang/en";
import fr from "../lang/fr";
import esp from "../lang/esp";

class Hpackages extends React.Component {

  constructor(props) {
    super(props);

   
      this.state = {
       
        ln: en,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            UK{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
              alt=""
            />
          </a>
        ),
        
      };
    }
    changelang(lan) {
      if (lan === "en") {
        this.setState({
          ln: en,
          flag: (
            <a
              className="nav-link dropdown-toggle"
              href="#"
              id="navbarDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              {" "}
              UK{" "}
              <img
                className="ml-1"
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
                alt=""
              />
            </a>
          ),
        });
       
      } else if (lan === "fr") {
        this.setState({
          ln: fr,
          flag: (
            <a
              className="nav-link dropdown-toggle"
              href="#"
              id="navbarDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              {" "}
              FR{" "}
              <img
                className="ml-1"
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/FR_d2vo8g.svg"
                alt=""
              />
            </a>
          ),
        });
      
      } else if (lan === "esp") {
        this.setState({
          ln: esp,
          flag: (
            <a
              className="nav-link dropdown-toggle"
              href="#"
              id="navbarDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              {" "}
              ESP{" "}
              <img
                className="ml-1"
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163562/OLLORUN_IMAGES/images/elements/ESP_ukit8i.svg"
                alt=""
              />
            </a>
          ),
        });
      
      }
    }
  render() {
    return (
      <div>
       <nav className="navbar navbar-expand-lg fixed-top">
          <a className="navbar-brand">
            <img
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_150,f_auto,q_auto/v1598163568/OLLORUN_IMAGES/images/elements/logo01_ollorun_ovoawo.png"
              alt=""
            />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span>
              <img
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/ham_y9sttw.svg"
                alt=""
              />
            </span>
          </button>
          <div
            className="collapse navbar-collapse justify-content-end"
            id="navbarNav"
          >
            <ul className="navbar-nav">
              <li
                className="nav-item active m-auto"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  
                  href="/#pannel-0"
                >
                  {this.state.ln[0]} <span className="sr-only">(current)</span>
                </a>
              </li>
              <li
                className="nav-item m-auto"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  
                  href="/#pannel-1"
                >
                  {this.state.ln[1]}
                </a>
              </li>
              <li
                className="nav-item m-auto"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  
                  href="/#pannel-2"
                >
                  {this.state.ln[2]}
                </a>
              </li>
              <li
                className="nav-item"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  target="_blank"
                  smooth={true}
                  duration={1000}
                  className="nav-link"
                  href={this.state.ln[151]}
                >
                  {this.state.ln[150]}
                </a>
              </li>
              <li
                className="nav-item text-center"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  className="nav-link"
                  target="_BLANK"
                  href="https://training.ollorun.com/index"
                >
                  TRAINING
                </a>
              </li>
              <li
                className="nav-item text-center"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  className="nav-link join-btn"
                  target="_BLANK"
                  href="https://backoffice.ollorun.com/"
                >
                  {this.state.ln[3]}
                </a>
              </li>
              <li className="nav-item dropdown">
                {this.state.flag}
                <div
                  className="dropdown-menu"
                  aria-labelledby="navbarDropdown"
                  data-toggle="collapse"
                  data-target="#navbarNav"
                >
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("en")}
                  >
                    UK{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
                      alt=""
                    />
                  </a>
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("fr")}
                  >
                    FR{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/FR_d2vo8g.svg"
                      alt=""
                    />
                  </a>
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("esp")}
                  >
                    ESP{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163562/OLLORUN_IMAGES/images/elements/ESP_ukit8i.svg"
                      alt=""
                    />
                  </a>
                </div>
              </li>
            </ul>
          </div>
        </nav>
     
        <section className="pack-top">
          <div className="container-fluid h-100 color-layer">
            <div className="row h-100 justify-content-center text-center">
              <div className="col-sm-8 text-center my-auto">
                <h1 className="text-align-center justify-content-center">
                  HEALTH PACKAGES
                </h1>
                <h4>AESTHETIC CARE WITHOUT SURGERY.</h4>
              </div>
              
            </div>
          </div>
          <div class="row">
            <a target="_blank" href="https://store.ollorun.com/index.php?route=product/category&path=61">
            <div className="col-sm-2 to-store-btn">
              Vist to Store
            </div></a>
          </div>
        </section>
        <section className=" mt-5 mb-5">
          <div className="container">
            <div className="row packs-view">
              <div class="col-6 col-sm-3 mt-5">
                <div class="card text-center product ">
                  <img className="card_img"
                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,w_600,q_auto/v1619163052/ollorun-health-packs/Ollorun_H_MICRONEEDLING_csiom0.png"
                    alt="MicroNeedling"
                  />
                  
                  <h5>MicroNeedling</h5>
               <div className="d-flex m-4 mb-3 mt-0">  <p class="price">129.00€</p>
                  <p className="pv">PV 60</p></div>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/index.php?route=product/product&path=61&product_id=29"
                  >
                    <button className="w-100">view</button>
                  </a>
                </div>
              </div>
              <div class="col-6 col-sm-3 mt-5">
                <div class="card text-center product">
                  <img className="card_img"
                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,w_600,q_auto/v1619163050/ollorun-health-packs/Ollorun_H_SB_cjfdds.png"
                    alt="Slim & Bum"
                  />
                  <h5>Slim & Bum</h5>
               <div className="d-flex m-4 mb-3 mt-0">  <p class="price">229.00€</p>
                  <p className="pv">PV 100</p></div>

                  <a
                    target="_blank"
                    href="https://store.ollorun.com/index.php?route=product/product&path=61&product_id=48"
                  >
                    <button className="w-100">view</button>
                  </a>
                </div>
              </div>
              <div class="col-6 col-sm-3 mt-5">
                <div class="card text-center product">
                  <img className="card_img"
                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,w_600,q_auto/v1619163052/ollorun-health-packs/Ollorun_H_MOISTURIZER_madlst.png"
                    alt="SUPER MOISTURIZER KIT"
                  />
                  <h5>Super Moisturizer Kit</h5>
               <div className="d-flex m-4 mb-3 mt-0">  <p class="price">60.00€</p>
                  <p className="pv">PV 25</p>
</div>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/index.php?route=product/product&path=61&product_id=28"
                  >
                    <button className="w-100">view</button>
                  </a>
                </div>
              </div>
              <div class="col-6 col-sm-3 mt-5">
                <div class="card text-center product">
                  <img className="card_img"
                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,w_600,q_auto/v1619163050/ollorun-health-packs/Ollorun_H_FY_u88hwm.png"
                    alt="Forever Young"
                  />
                  <h5>Forever Young</h5>
               <div className="d-flex m-4 mb-3 mt-0">  <p class="price">439.00€</p>
                  <p className="pv">PV 200</p>
</div>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/index.php?route=product/product&path=61&product_id=49"
                  >
                    <button className="w-100">view</button>
                  </a>
                </div>
              </div>
              <div class="col-6 col-sm-3 mt-5">
                <div class="card text-center product">
                  <img className="card_img"
                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,w_600,q_auto/v1619163050/ollorun-health-packs/Ollorun_H_MDB_u48pkf.png"
                    alt="My Daily Beauty"
                  />
                  <h5>My Daily Beauty</h5>
               <div className="d-flex m-4 mb-3 mt-0">  <p class="price">549.00€</p>
                  <p className="pv">PV 250</p>
</div>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/index.php?route=product/product&path=61&product_id=50"
                  >
                    <button className="w-100">view</button>
                  </a>
                </div>
              </div>
              <div class="col-6 col-sm-3 mt-5">
                <div class="card text-center product">
                  <img className="card_img"
                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,w_600,q_auto/v1619163051/ollorun-health-packs/Ollorun_H_ULTIMATE_nyzsla.png"
                    alt="Ultimate Pack"
                  />
                  <h5>Ultimate Pack</h5>
               <div className="d-flex m-4 mb-3 mt-0">  <p class="price">1250.00€</p>
                  <p className="pv">PV 500</p>
</div>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/index.php?route=product/product&path=61&product_id=51"
                  >
                    <button className="w-100">view</button>
                  </a>
                </div>
              </div>
              <div class="col-6 col-sm-3 mt-5">
                <div class="card text-center product">
                  <img className="card_img"
                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,w_600,q_auto/v1619163051/ollorun-health-packs/Ollorun_H_WW_czyq54.png"
                    alt="White & White"
                  />
                  <h5>White & White</h5>
               <div className="d-flex m-4 mb-3 mt-0">  <p class="price">149.00€</p>
                  <p className="pv">PV 75</p>
</div>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/index.php?route=product/product&path=61&product_id=47"
                  >
                    <button className="w-100">view</button>
                  </a>
                </div>
              </div>
              <div class="col-6 col-sm-3 mt-5">
                <div class="card text-center product">
                  <img className="card_img"
                    src="https://res.cloudinary.com/dmeppscpx/image/upload/f_auto,w_600,q_auto/v1619163050/ollorun-health-packs/Ollorun_H_DEMO_buejlf.png"
                    alt="Ollorun Demo Pack"
                  />
                  <h5>Demo Pack</h5>
               <div className="d-flex m-4 mb-3 mt-0">  <p class="price">129.00€</p>
                  <p class="pv">PV 0</p>
</div>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/index.php?route=product/product&path=61&product_id=58"
                  >
                    <button className="w-100">view</button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="container-fluid ">
          <div className="row footer">
            <div className="col-sm-4 mt-2">
              <div className="col text-center text-md-left">
                <img
                  className="footer-logo"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163568/OLLORUN_IMAGES/images/elements/logo01_ollorun_ovoawo.png"
                  alt=""
                />
              </div>
              <div className="footer-social text-center text-md-left">
                <a href="https://discord.gg/k7BaPZVKjy" target="_blank">
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163584/OLLORUN_IMAGES/images/elements/social/discord_czzaml.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.instagram.com/ollorun_health/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605208415/OLLORUN_IMAGES/images/elements/social/instagram_health_g3rnti.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.instagram.com/ollorun_wealth/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605208564/OLLORUN_IMAGES/images/elements/social/instagram_wealth_lu9qpo.svg"
                    alt=""
                  />
                </a>
                <a href="https://twitter.com/Ollorun1?s=09" target="_blank">
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163586/OLLORUN_IMAGES/images/elements/social/twitter_qjpxf3.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.facebook.com/Ollorun-Network-112311330515922/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163585/OLLORUN_IMAGES/images/elements/social/facebook_qhn2co.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.youtube.com/channel/UCKM2_TtWzVmLueKhdrSbEGQ"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605207157/OLLORUN_IMAGES/images/elements/social/youtube_jz4mah.svg"
                    alt=""
                  />
                </a>
              </div>
            </div>

            <div className="col-md-3 col-sm-6 mt-2">
              <ul>
                <li>
                  <a  href="/#pannel-0">
                    {this.state.ln[121]}
                  </a>
                </li>
                <li>
                  <a  href="/#pannel-1">
                    {this.state.ln[122]}
                  </a>
                </li>
                <li>
                  <a  href="/#pannel-2">
                    {this.state.ln[123]}
                  </a>
                </li>
                <li>
                  <a to="members" smooth={true} duration={1000} href="#">
                    {this.state.ln[124]}
                  </a>
                </li>
                <li>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/store/index.php?route=common/home"
                  >
                    {this.state.ln[125]}
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-md-3 col-sm-6 mt-2">
              <ul>
                <li>
                  <a
                    href="https://store.ollorun.com/store/index.php?route=information/information&information_id=5"
                    target="_blank"
                  >
                    {this.state.ln[126]}
                  </a>
                </li>
                <li>
                  <a
                    href="https://store.ollorun.com/store/index.php?route=information/information&information_id=3"
                    target="_blank"
                  >
                    {this.state.ln[127]}{" "}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[128]}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[129]}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[130]}
                  </a>
                </li>
              </ul>
            </div>

            <div className="row  text-md-left">
              <div className="col-sm-12 mt-4">
                <p>{this.state.ln[131]}</p>
                <p>{this.state.ln[132]}</p>
                <p>
                  <img src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163565/OLLORUN_IMAGES/images/elements/icon03_ollorun_tqiiju.svg" />
                  {this.state.ln[133]}
                </p>
              </div>
            </div>
          </div>
        </section>

         </div>
    );
  }
}
export default Hpackages;
