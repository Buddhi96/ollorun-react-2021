import logo from './logo.svg';
import './App.css';
import Home from './main/Home.jsx'
import Hpackages from './main/Hpackages.jsx'
import Wpackages from './main/Wpackages.jsx'
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';

function App() {
  return (
    <div >
    <Router>
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route  path="/helth-packs" component={Hpackages}></Route>
          <Route  path="/welth-packs" component={Wpackages}></Route>
        </Switch> 
      </Router>
  </div>
  );
}

export default App;
